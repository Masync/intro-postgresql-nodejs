CREATE DATABASE library;

CREATE TABLE books(
    section int,
    title text,
    author text
);

CREATE TABLE users(
    userName text,
    password text
);
INSERT INTO books VALUES
(1, 'Akelarre', 'Mario Mendoza');

INSERT INTO books VALUES
(2, 'Cobro de sangre', 'Mario Mendoza'),
(3, 'La melancolia de los feos', 'Mario Mendoza'),
(4, 'Scorpion city', 'Mario Mendoza'),
(5, 'Lady masacre', 'Mario Mendoza');


INSERT INTO users VALUES
('Masync', 'J990227$'),
('DaniG', 'Dani123%');


SELECT section, title, author FROM books;