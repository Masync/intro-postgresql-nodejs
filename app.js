const { Pool } = require('pg');
require('dotenv').config();  
const configDB =  {
    user: process.env.USERPG,
    host: process.env.HOST,
    password: '',
    database: process.env.DATABASE
};

const pool = new Pool(configDB);

const  getBook = async () => {
  const res = await pool.query('SELECT section, title, author FROM books');
  console.log(res.rows);
//   pool.end(); finalizar conexion
};

const insertBook = async () => {
    const query = 'INSERT INTO books(section, title, author) VALUES ($1,$2,$3)';
    const data = [1, 'Satanas', 'Mario Mendoza'];
    const q = await pool.query(query, data);
    console.log(q);
    
    pool.end();
};

const deleteBook = async () =>{
    const query = 'DELETE FROM  books WHERE title = $1';
    const title = ['Satanas'];
   await pool.query(query, title);
};

const updateBookSection = async () =>{
    const query = 'UPDATE books SET section = $1 WHERE section = $2';
    const section = [1, 2];
    await pool.query(query, section);
};
// insertBook();
// deleteBook();
// updateBookSection();
getBook();